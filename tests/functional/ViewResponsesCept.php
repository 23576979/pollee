<?php
  $I = new FunctionalTester($scenario);

  $I->am('researcher');
  $I->wantTo('View Responses');
  //when
  $I->amOnPage('/questionnaires');
  $I->see('Questionnaires', 'h2');
  //and
  $I->click('Login', 'a');

  //then
  $I->amOnPage('/login');
  $I->see('Login' ,'h3');
  $I->submitForm('.login', [
    'email' => 'jake@jakeleigh.io',
    'password' => 'password'
  ]);

  //then
  $I->seeCurrentUrlEquals('/myquestionnaires');
  $I->see('My Questionnaires');
  //and
  $I->click('Edit', 'a');

  //then
  $I->amOnPage('/myquestionnaires/1/edit');
  //and
  $I->see('.questionnaireTitle', 'h2');
  $I->click('View responses', 'a');

  //then
  $I->amOnPage('/myquestionnaires/1/responses');
  $I->see('.questionnaireTitle', 'h2');
  $I->see('Responses', 'h2');
?>