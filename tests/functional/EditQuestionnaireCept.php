<?php
  $I = new FunctionalTester($scenario);

  $I->am('researcher');
  $I->wantTo('Edit a questionnaire');

  //when
  $I->amOnPage('/questionnaires');
  $I->see('Questionnaires', 'h2');
  //and
  $I->click('Login', 'a');

  //then
  $I->amOnPage('/login');
  $I->see('Login' ,'h3');
  $I->submitForm('.login', [
    'email' => 'jake@jakeleigh.io',
    'password' => 'password'
  ]);

  //then
  $I->seeCurrentUrlEquals('/myquestionnaires');
  $I->see('My Questionnaires');

  //and
  $I->click('Edit', 'a');

  //then
  $I->amOnPage('/myquestionnaires/9/edit');
  //and
  $I->see('.questionnaireTitle', 'h2');

  //then
  $I->fillField('title', 'Updated Test Title');
  //and
  $I->click('Save Changes', 'a');

  //then
  $I->seeCurrentUrlEquals('/myquestionnaires');
  $I->see('My Questionnaires', 'h2');
  $I->see('Updated Test Title');
?>