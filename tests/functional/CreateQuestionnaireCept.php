<?php
  $I = new FunctionalTester($scenario);

  $I->am('researcher');
  $I->wantTo('Create a questionnaire');

  //when
  $I->amOnPage('/questionnaires');
  $I->see('Questionnaires', 'h2');
  //and
  $I->click('Login', 'a');

  //then
  $I->amOnPage('/login');
  $I->see('Login' ,'h3');
  $I->submitForm('.login', [
    'email' => 'jake@jakeleigh.io',
    'password' => 'password'
  ]);

  //then
  $I->seeCurrentUrlEquals('/myquestionnaires');
  $I->see('My Questionnaires');
  $I->dontSee('Create Questionnaire Test');

  //and
  $I->click('Create Questionnaire', 'div');

  //then
  $I->amOnPage('/myquestionnaires/edit/create');

  //and
  $I->see('Untitled Questionnaire', 'input');
  $I->submitForm('.createquestionnaire',[
    'title' => 'Test Questionnaire',
    'description' => 'Test Description',
    'questions' => [
      'question' => 'Test Question',
      'answer' => ['Test Answer', 'Another Test Answer']
    ]
  ])
?>