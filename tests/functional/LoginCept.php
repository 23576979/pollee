<?php
  $I = new FunctionalTester($scenario);
  $I->wantTo('Login');

  //when
  $I->amOnPage('/questionnaires');
  $I->see('Questionnaires', 'h2');
  //and
  $I->click('Login', 'a');

  //then
  $I->amOnPage('/login');
  $I->see('Login' ,'h3');
  $I->submitForm('.login', [
    'email' => 'jake@jakeleigh.io',
    'password' => 'password'
  ]);

  //then
  $I->seeCurrentUrlEquals('/myquestionnaires');
  $I->see('My Questionnaires');

?>