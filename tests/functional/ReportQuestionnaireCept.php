<?php
  $I = new FunctionalTester($scenario);

  $I->am('respondant');
  $I->wantTo('Report a Questionnaire');

  //when 
  $I->amOnPage('/questionnaires');
  $I->see('Questionnaires', 'h2');
  $I->see('Test Questionnaire', 'h4');
  //and
  $I->click('Report', 'a');

  //then
  $I->amOnPage('/questionnaires');
  $I->see('Questionnaires', 'h2');
  // //and
  $I->dontSee('Test Questionnaire', 'h4');
?>