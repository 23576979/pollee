<?php
  $I = new FunctionalTester($scenario);

  $I->am('researcher');
  $I->wantTo('Answer a questionnaire');

  //when 
  $I->amOnPage('/questionnaires');
  $I->see('Questionnaires', 'h2');
  //and
  $I->click('Answer', 'a');

  //then
  $I->amOnPage('/questionnaires/5');
  $I->see('Test Questionnaire', 'h2');
  $I->submitForm('.answerQuestionnaire', [
    'Question' => 'Test Question',
    'answer' => ['Test Answer 1']
  ]);
  
  //then
  $I->amOnPage('/questionnaires');
  $I->see('Questionnaires', 'h2');
?>