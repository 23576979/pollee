<?php
  $I = new FunctionalTester($scenario);

  $I->am('researcher');
  $I->wantTo('View download responses as a CSV file');

  //when
  $I->amOnPage('/reseacher/myquestionnaires');
  $I->see('My Questionnaires', 'h1');
  //and
  $I->click('Edit Questionnaire', 'a');

  //then
  $I->amOnPage('/researcher/myquestionnaires/1/edit');
  //and
  $I->see('.questionnaireTitle', 'h1');
  $I->click('View responses', 'a');

  //then
  $I->amOnPage('/researcher/myquestionnaires/1/responses');
  $I->see('.questionnaireTitle', 'h1');
  $I->see('Responses', 'h2');

  //and
  $I->click('Download CSV');
  //then
  $I->seeCurrentUrlEquals('/researcher/myquestionnaires/1/responses');
?>