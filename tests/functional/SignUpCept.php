<?php
  $I = new FunctionalTester($scenario);
  $I->wantTo('Sign Up');

  //when
  $I->amOnPage('/questionnaires');
  $I->see('Questionnaires', 'h2');
  
  //and
  $I->click('Register', 'a');

  //then
  $I->amOnPage('/register');
  $I->see('Register' ,'h3');
  $I->submitForm('.signup', [
    'first_name' => 'Bob',
    'last_name' => 'Burns',
    'email' => 'bobburns@test.com',
    'password' => 'bobburns123',
    'password-confirm' => 'bobburns123'
  ]);

  //then
  $I->seeCurrentUrlEquals('/myquestionnaires');
  $I->see('My Questionnaires');

?>