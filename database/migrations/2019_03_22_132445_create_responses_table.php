<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      //defining the fields and their data-types for the table in the db
        Schema::create('responses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('questionnaire_id')->unsigned();
            // $table->foreign('questionnaire_id')
            //     ->references('id')->on('questionnaire')
            //     ->onDelete('cascade');
            $table->integer('question_id')->unsigned();
            // $table->foreign('question_id')
            //     ->references('id')->on('question')
            //     ->onDelete('cascade');
            $table->integer('answer_id')->unsigned();
            // $table->foreign('answer_id')
            //     ->references('id')->on('answer')
            //     ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('responses');
    }
}
