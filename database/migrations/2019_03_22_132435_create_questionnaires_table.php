<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionnairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      //defining the fields and their data-types for the table in the db
        Schema::create('questionnaires', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('title');
            $table->string('description');
            $table->timestamp('date_created')->useCurrent();
            $table->boolean('is_flagged')->default(FALSE);
            $table->integer('created_by')->unsigned();
            // $table->foreign('created_by')
                // ->references('id')->on('users')
                // ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questionnaires');
    }
}
