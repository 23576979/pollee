<?php

use Illuminate\Database\Seeder;

class QuestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //seeding some dummy data in order to test functionality of site
        DB::table('questions')->insert([
            ['id' => 1, 'question' => 'Whats your favourite pet?', 'is_required' => TRUE, 'belongs_to_questionnaire' => 1],
            ['id' => 2, 'question' => 'Whats your preferred sea creature?', 'is_required' => TRUE, 'belongs_to_questionnaire' => 1],
            ['id' => 3, 'question' => 'Whats your Favouritiest 2 legged Aminal?', 'is_required' => TRUE, 'belongs_to_questionnaire' => 1],
            ['id' => 4, 'question' => 'Dog Breed?', 'is_required' => TRUE, 'belongs_to_questionnaire' => 1],
            ['id' => 5, 'question' => 'Do you like Snakes?', 'is_required' => TRUE, 'belongs_to_questionnaire' => 1],

            ['id' => 6, 'question' => 'Chinese Or Curry?', 'is_required' => TRUE, 'belongs_to_questionnaire' => 2],
            ['id' => 7, 'question' => 'Savoury Or Sweet?', 'is_required' => TRUE, 'belongs_to_questionnaire' => 2],
            ['id' => 8, 'question' => 'Do you like black pudding?', 'is_required' => TRUE, 'belongs_to_questionnaire' => 2],
            ['id' => 9, 'question' => 'Whats your favourite ice-cream flavour?', 'is_required' => TRUE, 'belongs_to_questionnaire' => 2],

            ['id' => 10, 'question' => 'Would you rather be?', 'is_required' => TRUE, 'belongs_to_questionnaire' => 3],
            ['id' => 11, 'question' => 'Would you rather?', 'is_required' => TRUE, 'belongs_to_questionnaire' => 3],

            ['id' => 12, 'question' => 'Football or Rugby?', 'is_required' => TRUE, 'belongs_to_questionnaire' => 4],
            ['id' => 13, 'question' => 'Which Country Has a Better Football Team?', 'is_required' => TRUE, 'belongs_to_questionnaire' => 4],
            ['id' => 14, 'question' => 'What is the goal of a questionnaire?', 'is_required' => TRUE, 'belongs_to_questionnaire' => 5],
            ['id' => 15, 'question' => 'When creating a questionnaire, what do you think is the most important feature?', 'is_required' => TRUE, 'belongs_to_questionnaire' => 5],
            ['id' => 16, 'question' => 'Do you think that the target audience is just as important as the questionnaire itself?', 'is_required' => TRUE, 'belongs_to_questionnaire' => 5],
            ['id' => 17, 'question' => 'If you were tasked with creating a questionnaire on a persons favourite colour, would you use open-ended or closed-ended questions?', 'is_required' => TRUE, 'belongs_to_questionnaire' => 5],
            ['id' => 18, 'question' => 'What is the main reason behind having open-ended questions?', 'is_required' => TRUE, 'belongs_to_questionnaire' => 5],
        ]);
    }
}
