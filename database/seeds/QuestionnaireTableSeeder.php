<?php

use Illuminate\Database\Seeder;

class QuestionnaireTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //seeding some dummy data in order to tezt functionality of site
        DB::table('questionnaires')->insert([
            ['id' => 1, 'title' => 'Favourite Animals?', 'description' => 'Simple questionnnaire to see the nations favourite animals', 'date_created' => '2019-01-14', 'is_flagged' => FALSE, 'created_by' => 1],
            ['id' => 2, 'title' => 'Favourite Foods', 'description' => 'Survey to compare the best of foods', 'date_created' => '2019-02-03', 'is_flagged' => FALSE, 'created_by' => 2],
            ['id' => 3, 'title' => 'Would You Rather', 'description' => 'Would you rather game', 'date_created' => '2018-12-12', 'is_flagged' => FALSE, 'created_by' => 1],
            ['id' => 4, 'title' => 'Questionnaire Of Sports', 'description' => 'Questionnaire to compare popular sports', 'date_created' => '2019-03-29', 'is_flagged' => FALSE, 'created_by' => 1],
            ['id' => 5, 'title' => 'Questionnaire Design', 'description' => 'Questionnaire to test students questionnaire design knowledge', 'date_created' => '2019-05-7', 'is_flagged' => FALSE, 'created_by' => 1],
        ]);
    }
}
