<?php

use Illuminate\Database\Seeder;

class AnswerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //seeding some dummy data in order to tezt functionality of site
      DB::table('answers')->insert([
        ['id' => 1, 'answer' => 'Dog', 'belongs_to_question' => 1],
        ['id' => 2, 'answer' => 'Cat', 'belongs_to_question' => 1],

        ['id' => 3, 'answer' => 'Great White Shark', 'belongs_to_question' => 2],
        ['id' => 4, 'answer' => 'Killer Whale', 'belongs_to_question' => 2],
        ['id' => 5, 'answer' => 'Dolphin', 'belongs_to_question' => 2],

        ['id' => 6, 'answer' => 'Man', 'belongs_to_question' => 3],
        ['id' => 7, 'answer' => 'Woman', 'belongs_to_question' => 3],

        ['id' => 8, 'answer' => 'Border-terrier', 'belongs_to_question' => 4],
        ['id' => 9, 'answer' => 'German Sheppard', 'belongs_to_question' =>  4],
        ['id' => 10, 'answer' => 'Lurcher', 'belongs_to_question' =>  4],
        ['id' => 11, 'answer' => 'Pug', 'belongs_to_question' => 4],
        ['id' => 12, 'answer' => 'Other', 'belongs_to_question' => 4],

        ['id' => 13, 'answer' => 'Yes', 'belongs_to_question' => 5],
        ['id' => 14, 'answer' => 'No', 'belongs_to_question' => 5],

        ['id' => 15, 'answer' => 'Chinese', 'belongs_to_question' => 6],
        ['id' => 16, 'answer' => 'Curry', 'belongs_to_question' => 6],
        ['id' => 17, 'answer' => 'Neither', 'belongs_to_question' => 6],

        ['id' => 18, 'answer' => 'Savoury', 'belongs_to_question' => 7],
        ['id' => 19, 'answer' => 'Sweet', 'belongs_to_question' => 7],
        ['id' => 20, 'answer' => 'Neither', 'belongs_to_question' => 7],

        ['id' => 21, 'answer' => 'Yes', 'belongs_to_question' => 8],
        ['id' => 22, 'answer' => 'No', 'belongs_to_question' => 8],

        ['id' => 23, 'answer' => 'Chocolate', 'belongs_to_question' => 9],
        ['id' => 24, 'answer' => 'Mint', 'belongs_to_question' => 9],
        ['id' => 25, 'answer' => 'Strawberry', 'belongs_to_question' => 9],
        ['id' => 26, 'answer' => 'Toffee Crisp', 'belongs_to_question' => 9],

        ['id' => 27, 'answer' => 'See The Past', 'belongs_to_question' => 10],
        ['id' => 28, 'answer' => 'See The Future', 'belongs_to_question' => 10],

        ['id' => 29, 'answer' => 'Drown', 'belongs_to_question' => 11],
        ['id' => 30, 'answer' => 'Burn Alive?', 'belongs_to_question' => 11],

        ['id' => 31, 'answer' => 'Football', 'belongs_to_question' => 12],
        ['id' => 32, 'answer' => 'Rugby', 'belongs_to_question' => 12],

        ['id' => 33, 'answer' => 'England', 'belongs_to_question' => 13],
        ['id' => 34, 'answer' => 'Spain', 'belongs_to_question' => 13],
        ['id' => 35, 'answer' => 'Brazil', 'belongs_to_question' => 13],
        ['id' => 36, 'answer' => 'Portugal', 'belongs_to_question' => 13],

        ['id' => 37, 'answer' => 'To gather information from respondents', 'belongs_to_question' => 14],
        ['id' => 38, 'answer' => 'To perform market research for a particular field', 'belongs_to_question' => 14],

        ['id' => 39, 'answer' => 'Its short', 'belongs_to_question' => 15],
        ['id' => 40, 'answer' => 'The questions flow from one another', 'belongs_to_question' => 15],
        ['id' => 41, 'answer' => 'Well designed', 'belongs_to_question' => 15],
        ['id' => 42, 'answer' => 'Clear and consise', 'belongs_to_question' => 15],
        
        ['id' => 43, 'answer' => 'Yes', 'belongs_to_question' => 16],
        ['id' => 44, 'answer' => 'No', 'belongs_to_question' => 16],

        ['id' => 45, 'answer' => 'Open', 'belongs_to_question' => 17],
        ['id' => 46, 'answer' => 'Closed', 'belongs_to_question' => 17],

        ['id' => 47, 'answer' => 'Gives the user the ability to add more detail to their answers', 'belongs_to_question' => 18],
        ['id' => 48, 'answer' => 'ALlows the user to choose their own answer as appose to a pre-defined answer', 'belongs_to_question' => 18],

    ]);
    }
}
