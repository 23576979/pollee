<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class question extends Model
{
  //defining the fields that are able to be created
  protected $fillable = [
    'question',
    'is_required',
    'belongs_to_questionnaire',
];

    //defining relationships between questions and questionnaires, and questions and answers, and questions and responses
    public function questionnaire(){
        return $this->belongsTo('App\questionnaire');
    }

    public function answers(){
        return $this->hasMany('App\answer');
    }
    public function responses(){
        return $this->belongsToMany('App\response');
    }
}
