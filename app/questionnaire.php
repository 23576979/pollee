<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class questionnaire extends Model
{
  //defining the fields that are able to be created
    protected $fillable = [
        'title',
        'description',
        'created_by',
        'is_flagged',
    ];

    //defining relationships between questionnaires and users, and questionnaires and responses, and questionnaires and questions
    public function users(){
        return $this->belongsTo('App\user');
    }

    public function questions(){
        return $this->hasMany('App\question');
    }

    public function responses(){
        return $this->belongsToMany('App\response');
    }
}
