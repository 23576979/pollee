<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

class user extends Model implements Authenticatable
{
    use AuthenticableTrait;

    //defining the fields that are able to be created
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password'
    ];

     //defining relationships between users and questionnaires
    public function questionnaires(){
        return $this->hasMany('App\questionnaire');
    }
}
