<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Questionnaire;
use App\Question;
use App\Response;
use App\Answer;



class QuestionnaireController extends Controller
{
  protected $questArray;

    /*
   * Secure the set of pages to the admin.
   */
  
  public function __construct()
  {
      $this->middleware('auth');
      $this->questArray = array(
        0 => array(
          'answers' => array('', ''),
          'question' => ""
        )
      );
  }
    /**
     * Display a listing of the resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //index function for when the user loads the /myquestionnaires url
    public function index()
    {
        //find the questionnaires where the created_by field matches the id of the user that is currently logged in
        $questionnaires = Questionnaire::where('created_by', Auth::user()->id)->get();
        //return the myquestionnaires view with the questionnaires array passed in to allow looping through each questionnaire and displaying it
        return view('researcher.myquestionnaires', ['questionnaires' => $questionnaires]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $questArray = $this->questArray;

        //take the fields from the questionnaire, question and answer tables that are needed in order to create a questionnaire such as the title, description, questions and answers and store that within a variables so they can be accessed in the view
        $questionnaire = Questionnaire::pluck('id', 'title', 'description', 'created_by');
        $questions = Question::pluck('id', 'question', 'is_required', 'belongs_to_questionnaire');
        $answers = Answer::pluck('id', 'answer', 'belongs_to_question');      

        //return the createquestionnaire view and pass in the questionnaire, question and answer variables
        return view('researcher.createquestionnaire', compact('questionnaire', 'questions', 'answers', 'questArray' ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //function to store a newly created questionnaire
    public function store(Request $request)
    {
      //create a new questionnaire instance using the title, description and the current user from the request as the data for the questionnaire
      $questionnaire = Questionnaire::create([
        'title' => request('title'),
        'description' => request('description'),
        'created_by' => Auth::user()->id
      ]);  

      //get the answers, questions from the request input fields which hold the data entered by the user
      $answers = request('answer');
      $questions = request('question');

      //foreach question in the request, create a new question using the question and belongs_to_questionnaire from the request as the data for the question 
      foreach($questions as $key =>$question){
        $questionArray = new Question;
        $questionArray->question = $question;
        $questionArray->belongs_to_questionnaire = $questionnaire->id;
        $questionArray->save();
      }
        //foreach answer in the request, create a new answer using the answer and belongs_to_question from the request as the data for the answer 
        foreach($answers as $answer){
            $answerArray = new Answer;
            $answerArray->answer = $answer;
            $answerArray->belongs_to_question = $questionArray->id;
            $answerArray->save();
          }
    
        //then redirect back to the myquestionnaires page with the success page to show the user that a questionnaire has been created
        return redirect('/myquestionnaires')->with('questionnaireAdded', 'Questionnaire Created');
  }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      //join to get all of the responses where the field 'questionnaire_id' in the repsponse table matches the id that is passed in through the show function. 
      //also joined the question and answer data relating to the questionnaire to use the question and answer title
      $responses = DB::table('responses')
        ->select('responses.id', 'responses.questionnaire_id', 'responses.question_id', 'responses.answer_id', 'questionnaires.title', 'questions.question', 'answers.answer')
        ->join('questionnaires', 'responses.questionnaire_id', '=', 'questionnaires.id')
        ->join('questions', 'responses.question_id', '=', 'questions.id')
        ->join('answers', 'responses.answer_id', '=', 'answers.id')
        ->where('responses.questionnaire_id', '=', $id)
        ->get();

      //individual answers join that relate to the questionnaire reponse that is being passed
      $answers = DB::table('answers')
        ->select('answers.id', 'answers.answer', 'answers.belongs_to_question')
        ->join('responses', 'answers.id', '=', 'responses.answer_id')
        ->where('responses.questionnaire_id', '=', $id)
        ->get();

      //this join just gets each answer that relates to the questionnaire in order to show once instance of the answer rather than however many times the answer has been chosen when creating tables to show the number of times answers have been chosen
      $options = DB::table('answers')
        ->select('answers.id', 'answers.answer', 'answers.belongs_to_question')
        ->join('questions', 'answers.belongs_to_question' , 'questions.id')
        ->get();

      //get the first questionnaire where the id field matches the id that is passed into the show function
      $questionnaire = Questionnaire::where('id', $id)->first();
      //get the questions where the 'belongs_to_questionnaire' field matches the id that is passed into the show function
      $questions = Question::where('belongs_to_questionnaire', $id)->get();

      //return the viewResponses view with the responses, answers, options arrays passed in and the questionnaire and question data also passed in
      return view('researcher/viewResponses', ['responses' => $responses, 'answers' => $answers, 'options' => $options])->withQuestionnaire($questionnaire)->withQuestions($questions);
    }

    //export the responses to open in excel function
    public function export($id)
    {
      //join to get all of the responses where the field 'questionnaire_id' in the repsponse table matches the id that is passed in through the show function. 
      //also joined the question and answer data relating to the questionnaire to use the question and answer title
      $responses = DB::table('responses')
        ->select('responses.id', 'responses.questionnaire_id', 'responses.question_id', 'responses.answer_id', 'questionnaires.title', 'questions.question', 'answers.answer')
        ->join('questionnaires', 'responses.questionnaire_id', '=', 'questionnaires.id')
        ->join('questions', 'responses.question_id', '=', 'questions.id')
        ->join('answers', 'responses.answer_id', '=', 'answers.id')
        ->where('responses.questionnaire_id', '=', $id)
        ->get();

      //individual answers join that relate to the questionnaire reponse that is being passed
      $answers = DB::table('answers')
        ->select('answers.id', 'answers.answer', 'answers.belongs_to_question')
        ->join('responses', 'answers.id', '=', 'responses.answer_id')
        ->where('responses.questionnaire_id', '=', $id)
        ->get();

      //this join just gets each answer that relates to the questionnaire in order to show once instance of the answer rather than however many times the answer has been chosen when creating tables to show the number of times answers have been chosen
      $options = DB::table('answers')
        ->select('answers.id', 'answers.answer', 'answers.belongs_to_question')
        ->join('questions', 'answers.belongs_to_question' , 'questions.id')
        ->get();

      //get the first questionnaire where the id field matches the id that is passed into the show function
      $questionnaire = Questionnaire::where('id', $id)->first();
      //get the questions where the 'belongs_to_questionnaire' field matches the id that is passed into the show function
      $questions = Question::where('belongs_to_questionnaire', $id)->get();

      //create a header with the filename being the questionnaire title in order to start an automatic download when the user loads the /export/{id} url
      $header = 'Content-Disposition: attatchment;filename=' . $questionnaire->title. '.xls';
      header($header);

      //return the viewResponses view with the responses, answers, options arrays passed in and the questionnaire and question data also passed in
      return view('researcher/export', ['responses' => $responses, 'answers' => $answers, 'options' => $options])->withQuestionnaire($questionnaire)->withQuestions($questions);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      //join to select the answers and other fields such as the answer as well as the id that relates to the question which in turn matches 'belongs_to_questionnaire' field to the id that is passed in through the edit function
      $answers = DB::table('answers')
        ->select('answers.id', 'answers.answer', 'answers.belongs_to_question', 'questions.question', 'questions.is_required', 'questions.belongs_to_questionnaire')
        ->join('questions', 'answers.belongs_to_question', '=', 'questions.id')
        ->join('questionnaires', 'questions.belongs_to_questionnaire', '=', 'questionnaires.id')
        ->where('questionnaires.id', '=', $id)
        ->get();

        //join to select the questions and the relative fields where the 'belongs_to_questionnaire' field matches the id passed in through the edit function
        $questions = DB::table('questions')
          ->select('questions.id', 'questions.question', 'questions.is_required', 'questions.belongs_to_questionnaire', 'questionnaires.title', 'questionnaires.description', 'questionnaires.date_created', 'users.first_name', 'users.last_name')
          ->join('questionnaires', 'questions.belongs_to_questionnaire', '=', 'questionnaires.id')
          ->join('users', 'questionnaires.created_by', '=', 'users.id')
          ->where('questionnaires.id', '=', $id)
          ->get();


        //gets the first questionnaire where the id matches the id passed into the function
        $questionnaire = Questionnaire::where('id', $id)->first();

        //return the editquestionnaire view along with the answers and questions array and the questionnaire data in order to show the current answer, question and questionnaire data in order for the user to edit any of the data
        return view('researcher.editquestionnaire', ['answers' => $answers, 'questions' => $questions])->withQuestionnaire($questionnaire);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      //insert into the questionnaire table 
      DB::table('questionnaires')
      //where the questionnaire id field matches the id parameter in the update function
      ->where('id', $id)
      //update the title and description field with the title and description name from the request in the view
      ->update(array(
                'title'=>$request->get('title'),
                'description'=>$request->get('description')
       ));
       
       //get the array of questions that hold the text and the array of question ids
       $questions = request('question');
       $question_ids = request('question_id');

       //loop through each question with an index
       foreach($questions as $key => $question){
         //insert in the questions table
        DB::table('questions')
        //where the id matches the current questionid using the index 
        ->where('id', $question_ids[$key])
        //update the question field with the new question text
        ->update(array(
                  'question'=> $question
         ));
       }

       //get the array of answers that hold the text and the array of answer ids
       $answers = request('answer');
       $answer_ids = request('answer_id');

       //loop through each answer with an index
       foreach($answers as $key => $answer){
         //insert in the answers table
        DB::table('answers')
        //where the id matches the current answerid using the index 
        ->where('id', $answer_ids[$key])
        //update the answer field with the new answer text
        ->update(array(
                  'answer'=> $answer
         ));
       }
       
       //redirect back to the myquestionnaires page with a success message saying the questionnaire has been updated
       return redirect('/myquestionnaires')->with('success', 'Questionnaire Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        //find the questions where the 'belongs_to_questionnaire' field matches the id that is passed on the destroy function
        $questions = Question::where('belongs_to_questionnaire', $id);
        //delete the questions
        $questions->delete();
        //find the questions where the 'belongs_to_questionnaire' field matches the id that is passed on the destroy function
        $questionnaire = Questionnaire::findOrFail($id);
        //delete the questionnaire
        $questionnaire->delete();
        
        //redirect back to the myquestionnaires page with the success message saying questionnaire has been deleted
        return redirect('/myquestionnaires')->with('questionnairedeleted', 'Questionnaire Deleted');
    }

    public function reportedQuestionnaires(){
      //find the questionnaires where the created_by field matches the id of the user that is currently logged in
        $questionnaires = Questionnaire::where('is_flagged', 1)->get();
        // return the myquestionnaires view with the questionnaires array passed in to allow looping through each questionnaire and displaying it
        return view('admin.reported', ['questionnaires' => $questionnaires]);
    }

    public function revoke($id){
      DB::table('questionnaires')
        //where the id matches the current answerid using the index 
        ->where('id', $id)
        //update the is_flagged field to be False or 0
        ->update(array(
                  'is_flagged'=> 0
         ));

        return redirect('/admin/reported_questionnaires')->with('revoke', 'Report Claim Revoked');
    }

    //function to handle an admin deleting a questionnaire that has been reported
    public function deleteReported($id){
      //find the questionnaire that matches the id field with the variable $id that is passed into the function
      $questionnaire = Questionnaire::where('id', $id);
      //delete that questionnaire from the db
      $questionnaire->delete();
      //find the questions where the 'belongs_to_questionnaire' field matches the id passed into the function
      $questions = Question::where('belongs_to_questionnaire', $id);
      //delete those questions from the db      
      $questions->delete();

      //redirect back to the report questionnaires page with the message informing the admin that the questionnaire has been deleted
      return redirect('/admin/reported_questionnaires')->with('success', 'Questionnaire Deleted');
    }
}
