<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Questionnaire;
use App\Response;
use App\Question;
use App\Answer;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){

        //joins to retrieve all of the questionnaires and attatch the questionnaire title and description
        $questionnaires = DB::table('questionnaires')
        ->select('questionnaires.id', 'questionnaires.title', 'questionnaires.description', 'users.first_name', 'users.last_name')
        ->join('users', 'questionnaires.created_by', '=', 'users.id')
        ->where('is_flagged', '=', 0)
        ->get();
        return view('home', ['questionnaires' => $questionnaires]);
    }

      /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      //join to select all of the answers that have the 'belongs_to_question' field matching the id of the question that matches the 'belongs_to_questionnaire' field that was clicked on given as a parameter 'id' while attaching the answer in order for the answer to be displayed on the page rather than just the id of the answer
      $answers = DB::table('answers')
        ->select('answers.id', 'answers.answer', 'answers.belongs_to_question', 'questions.question', 'questions.is_required', 'questions.belongs_to_questionnaire')
        ->join('questions', 'answers.belongs_to_question', '=', 'questions.id')
        ->join('questionnaires', 'questions.belongs_to_questionnaire', '=', 'questionnaires.id')
        ->where('questionnaires.id', '=', $id)
        ->get();

        //join to select all of the questions where its the questionnaire.id field that is a primary key of the questions 'belongs_to_questionnaire' field is the id that is passed into the show function to retreive all of the correct questions along with their question and is required field 
        $questions = DB::table('questions')
        ->select('questions.id', 'questions.question', 'questions.is_required', 'questions.belongs_to_questionnaire', 'questionnaires.title', 'questionnaires.description', 'questionnaires.date_created', 'users.first_name', 'users.last_name')
        ->join('questionnaires', 'questions.belongs_to_questionnaire', '=', 'questionnaires.id')
        ->join('users', 'questionnaires.created_by', '=', 'users.id')
        ->where('questionnaires.id', '=', $id)
        ->get();


        //get the first questionnaire where the id matches the id passed in through the show function
        $questionnaire = Questionnaire::where('id', $id)->first();

        //return to the answer view and pass in the array of question, answers and the questionnaire in order to loop through each and display on page
      return view('answer', ['answers' => $answers, 'questions' => $questions])->withQuestionnaire($questionnaire);
    }

      /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request){

      //get the array of answer ids and question ids passed in through the request parameter
      $answers = request('answer_id');
      $questions = request('question_id');
      
        //loop through each question using an index to track the iteration num
        foreach($questions as $key => $question){
          //create a new repsonse entry called responseQuestions
          $responseQuestions = new Response;
          //assign the value of the questionnaire_id field as the questionnaire id that was passed through the request using the name 'questionnaire_id' and giving it a value of the questionnaire id that the page is currently showing
          $responseQuestions->questionnaire_id = request('questionnaire_id');
          //assign the value of the question_id to be the current question id that the loop is passign through
          $responseQuestions->question_id = $question;
          //assign the value of the answer_id as the value of the answers id using the index incremented through each loop
          $responseQuestions->answer_id = $answers[$key];
          //save each reponse
          $responseQuestions->save();
    }
        //redirect back to the questionnaires page with the success message passed in telling the user a reponse has been saved
        return redirect('/questionnaires')->with('success', 'Response Saved');
    }

    //admin function used to retrieve all of the users 
    public function admin(){
      //select all of the users and store within an array
      $users = User::all();

      //return to the view admin home page with the array passed in to allow for looping over each user and creating a field in a table
      return view('/admin/home', ['users' => $users]);
    }

    public function destroy($id)
    {
        //when the admin clicks delete user over a specific user, the id is passed and the user field in the db with the correct is selected and deleted fron the db
        $user = User::findOrFail($id);
        $user->delete();

        //the user is then redirected back to the same page with a success messagign saying user has been deleted
        return redirect('/admin')->with('success', 'User Deleted');
    }

    public function report($id){

      DB::table('questionnaires')
      //where the id matches the current questionid using the index 
      ->where('id', $id)
      //update the is_flagged field to be true or 1
      ->update(array(
                'is_flagged'=> 1
       ));
          // //joins to retrieve all of the questionnaires and attatch the questionnaire title and description
          return redirect('/questionnaires')->with('reported', 'Questionnaire Reported');
    }
}