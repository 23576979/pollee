<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\User;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        //user must be logged on to view page
        $this->middleware('auth');
    }

    //when the profile page is first loaded
    public function index()
    {
        //select the user in the db where the id field matches the id of the user that is currently logged on in the session
        $user = User::where('id', Auth::user()->id)->get();
        //load the profile page with the user details passed in in order to show user data such as email, name
        return view('researcher.myprofile', ['user' => $user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
      //select the name in the view called userId which holds the users id and store it in a variable called userId
      $userId = request('userId');
      //find the user where the id field in the db matches the id in the view
      $user = User::find($userId);

      //update the fields of the user in the db with the input with the name firstName, lastName and email that gets passed in through the request
      $user->first_name = $request->firstName;
      $user->last_name = $request->lastName;
      $user->email = $request->email;
      //save the details in the field
      $user->save();

      //redirect back to the same page to show the changes along with a message telling the user that the profile has been updated
      return redirect('/myprofile')->with('success', 'Profile Updated');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      //find the user in the db where the id field matches the id that is passed in through the delete function
      $user = User::findOrFail($id);
      //delete the user
      $user->delete();

      //redirect back to the home page
      return redirect('/questionnaires');
    }
}
