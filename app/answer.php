<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class answer extends Model
{
  //defining the fields that are able to be created
  protected $fillable = [
    'answer',
    'belongs_to_question',
];

    //defining relationships between answers and questions, and answers and responses
    public function question(){
        return $this->belongsTo('App\question');
    }
    public function responses(){
        return $this->belongsToMany('App\response');
    }
}
