<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class response extends Model
{
  //defining the fields that are able to be created
  protected $fillable = [
    'questionnaire_id',
    'question_id',
    'answer_id'
];

    //defining relationships between responses and questionnaires, and responses and questions, and responses and answers
    public function questionnaires(){
        return $this->hasMany('App\questionnaire');
    }
    public function questions(){
        return $this->hasMany('App\question');
    }

    public function answers(){
        return $this->hasMany('App\answer');
    }
}
