@extends('layouts.master')

@section('title', 'Pollee')

@section('content')
<div class='flex-container'>
  <div class='questionnaire-cards'>
      @if(\Session::has('success'))
      <div class='alert alert-success'>
      {{\Session::get('success')}}
      </div>
      @endif
      @if(\Session::has('revoke'))
      <div class='alert alert-success'>
      {{\Session::get('revoke')}}
      </div>
      @endif
      <!-- if there are questionnaires in the db then create a title and a search box-->
    @if (isset ($questionnaires))
        <div class='home-title-section'>
          <h2>Reported Questionnaires</h2>
          </div>
          <!-- foreach questionnaire in the db create a new card and add the title, description and created by field-->
          @foreach ($questionnaires as $questionnaire)
          <div id='title' class="ui fluid raised questionnaire-card card">
            <div id='questionnaireTitle' class='card-header'>
              <h4 >{{ $questionnaire->title}}</h4>
              </div>
                <div class="card-body">
                <div class='flex-card'>
                  <p class="card-text">{{ $questionnaire->description}}</p>
                </div>  
              </div>
              <div class="extra content">
                  <a href='/admin/revoke/reported_questionnaire/{{ $questionnaire->id}}' class="answer-btn small ui green button" name={{ $questionnaire->title }}>Revoke</a>

              {{ Form::open(array('url' => 'admin/reported_questionnaires/' . $questionnaire->id)) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit('Delete', array('class' => 'btn small ui red button')) }}
              {{ Form::close() }}

                  <div class="right floated author">
                      <p class='card-text created-by text-right'>{{ $questionnaire->first_name}} {{ $questionnaire->last_name}}</p>
                  </div>
                </div>
            </div>
          @endforeach
    @else
      <p> no questionnaires added yet</p>
    @endif
  </div>
</div>
  @endsection