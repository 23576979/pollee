@extends('layouts.master')

@section('title', 'Pollee')

@section('content')
<div class='flex-container'>
  <div class='questionnaire-cards'>
      @if(\Session::has('success'))
      <div class='alert alert-success'>
      {{\Session::get('success')}}
      </div>
      @endif
    <h2>Admin Page</h2>
    <br>
      <table class="ui unstackable table">
          <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">First Name</th>
              <th scope="col">Last Name</th>
              <th scope="col">Email Address</th>
              <th scope='col'></th>
            </tr>
          </thead>
          <tbody>
            <!-- foreach user create a new row inside the table
              with each field being the firstname, lastname and email -->
            @foreach ($users as $user)
            <tr>
            <th scope="row">{{$user->id}}</th>
              <td>{{$user->first_name}}</td>
              <td>{{$user->last_name}}</td>
              <td>{{$user->email}}</td>
              <td>{{ Form::open(array('url' => '/admin/' . $user->id)) }}
                  {{ Form::hidden('_method', 'DELETE') }}
                  {{ Form::submit('Delete', array('class' => 'btn small ui red button')) }}
              {{ Form::close() }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
  </div>
</div>
  @endsection