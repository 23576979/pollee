@extends('layouts.master')

@section('title', 'Pollee')

@section('content')
<div class='flex-container'>
  <div class='questionnaire-cards'>
      @if(\Session::has('success'))
      <div class='alert alert-success'>
      {{\Session::get('success')}}
      </div>
      @endif
  {!! Form::open(array('action' => 'ProfileController@store', 'id' => 'saveuserdetails')) !!}
  @if(isset($user))
  <!-- a foreach loop is used even though theres only ever one user for the profile page because it allows for the properties of the user to be accessed such as the name, email-->
  @foreach($user as $data)
  <input type='hidden' name='userId' value={{$data->id}}></input>
    <h2>{{$data->first_name}}s Profile</h2>
    <br>
    <form class='profile-form ui form center'>
        <div class="field">
          <label for="firstName" class="col-sm-3 col-form-label">First Name</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" name="firstName" value={{$data->first_name}}></input>
          </div>
        </div>
        <div class="field">
          <label for="lastName" class="col-sm-3 col-form-label">Last Name</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" name="lastName" value={{$data->last_name}}>
          </div>
        </div>
        <div class="field">
            <label for="email" class="col-sm-3 col-form-label">Email</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="email" value={{$data->email}}>
            </div>
          </div>
          <fieldset class="form-group">
            <div class="field">
              <label for="password" class="col-sm-3 col-form-label">Password</label>
              <div class="col-sm-9">
                <input type="password" class="form-control" name="password" placeholder='Enter Current Password'></input>
              </div>
            </div>
            <div class="field">
                <label for="password" class="col-sm-3 col-form-label"></label>
                <div class="col-sm-9">
                  <input type="password" class="form-control" name="password" placeholder='Enter New Password'></input>
                </div>
              </div>
              <div class="field">
                  <label for="password" class="col-sm-3 col-form-label"></label>
                  <div class="col-sm-9">
                    <input type="password" class="form-control" name="password" placeholder='Re-type New Password'></input>
                  </div>
                </div>
          </fieldset>
          {!! Form::submit('Save Changes', ['class' => 'submit-questionnaire-btn small ui blue button']) !!}
            {!! Form::close() !!}
          {!! Form::close() !!}
        <div class="form-group row">
          <div class="col-sm-12">
            {{ Form::open(array('url' => 'myprofile/' . $data->id)) }}
            {{ Form::hidden('_method', 'DELETE') }}
            {{ Form::submit('Delete Account', array('class' => 'btn small ui red button')) }}
            {{ Form::close() }}
        
            
          </div>
        </div>
      </form>
    @endforeach
    @endif

  </div>
</div>
  @endsection