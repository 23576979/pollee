@extends('layouts.master')

@section('title', 'Edit Questionnaire')

@section('content')
{!! Form::model($questionnaire, ['method' => 'PATCH', 'url' => 'myquestionnaires/edit/' . $questionnaire->id]) !!}
{{ csrf_field() }}
<div class='flex-container'>
  <div class='questionnaire-cards'>
    <div class='row title-div'>
      <input type='text' name='title' class='questionnaire-title-field' value='{{$questionnaire->title}}'></input>
      <div class='create-button'>
        <a id='moreFields' onClick=(moreFields()) class='small ui blue button'>Add Question</a>
      </div>
    </div>
    <div class='row title-div'>
      <input type='text' name='description' class='questionnaire-description-field' value='{{$questionnaire->description}}'></input>
    </div>
    <!-- foreach question create a new card that holds the question title and another loop for each answer -->
    @foreach($questions as $question)
      <input name="question_id[]" type="hidden" value={{ $question->id}}>
        <div class='ui raised fluid card'>
            <div class='card-header'>
              <input type='text' name='question[]' class='question-title-field' value='{{$question->question}}'>
            </div>
            <div class='card-body'>
              <!-- foreach answer where the belongs_to_question field matches the id of the question its looping inside of then create an input for each answer -->
            @foreach($answers as $answer)
              @if($answer->belongs_to_question == $question->id)
              <input name="answer_id[]" type="hidden" value={{ $answer->id}}>
              <div class='option'>
                <input type='radio' name='answers' value={{$answer->id}} disabled>
                <input type='text' name='answer[]' class='option-field' value='{{$answer->answer}}''>
                <span> X </span>
              </div>
              @endif
            @endforeach
            <button class='tiny ui basic blue button'>Add Option</button>
            <button class='tiny ui basic red button'>Delete Option</button>
            <button class="ui icon basic button right floated">
                <i class="trash icon"></i>
              </button>
          </div>
        </div>
      @endforeach
    {!! Form::submit('Update Questionnaire', ['class' => 'submit-questionnaire-btn small ui blue button']) !!}
{!! Form::close() !!}
</div>
</div>
@endsection
