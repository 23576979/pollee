@extends('layouts.master')

@section('title', 'Responses')

@section('content')
<div class='flex-container'>
  <div class='questionnaire-cards'>
    <div class='row title-div'>
      <h2>Responses For - {{ $questionnaire->title }}</h2>
      
    </div>
    <div class='description-div'>
      <p>{{ $questionnaire->description}}</p>
      <a class='small ui green button ' href='export/{{$questionnaire->id}}'>Download Responses</a>
    </div>
    <!-- foreach question create a new card that holds the question title and another loop for each option available per question-->
    @foreach ($questions as $question)
    <div class='ui fluid raised card'>
        <div class="content">
            <div class="header"> 
              <h3>{{$question->question}}</h3>
            </div>
          </div>
     <div class='content'>
    <table class="ui celled table">
      <thead>
        <tr>
          <th>Answer</th>
          <th>Number Of Times Chosen</th>
        </tr>
      </thead>
      <tbody>
        <!-- for each option add the answer inside a new field in a table -->
        @foreach ($options as $option)
          @if ($option->belongs_to_question == $question->id)
          <tr>
            <td data-label="Answer">{{$option->answer}}</td>
            <td data-label='Amount'>
                <!-- a hidden input purely to hold an int that can be added to for each time an answer is chosen, e.g. the number of times it matches the option id-->
                <input name="invisible" type="hidden" value={{$num = 0}}>
              <!-- another loop that interates over the number of responses for a particular answer -->
              @for ($i=0; $i<count($responses); $i++)
               <!--if the current response answer_id field matches the id field of the option that it is currently looping inside of then  add 1 to the num variable which will track the number of times an answer is chosen-->
                @if($responses[$i]->answer_id == $option->id)
                <input name="invisible" type="hidden" value={{$num++}}>
                @endif
              @endfor
              <!-- after the loop has finished output the value of num inside of the field-->
              {{$num}}
            </td>
            @endif
            @endforeach 
        </tr>
      </tbody>
    </table>
  </div>
  </div>
    @endforeach
 
  </div>
</div>
@endsection

