@extends('layouts.master')

@section('title', 'Create Questionnaire')

@section('content')
<div class='flex-container'>
    @if(\Session::has('questionnaireAdded'))
    <div class='alert alert-success'>
    {{\Session::get('questionnaireAdded')}}
    </div>
    @endif
  <div class='questionnaire-cards'>
    {!! Form::open(array('action' => 'QuestionnaireController@store', 'id' => 'createquestionnaire')) !!}
    <div class='row title-div'>
      <input type='text' name='title' class='questionnaire-title-field' placeholder='Untitled Questionnaire'>   
      <div class='create-button'>
        <a id='moreFields' onClick=(addQuestions()) class='small ui blue button'>Add Question</a>
      </div>
    </div>
    <div class='row title-div'>
      <input type='text' name='description' class='questionnaire-description-field' placeholder='Untitled Description'>  
    </div>
      <div id='question-card' class="ui raised fluid card">
       <div class='card-header'>
          <div class='question'>
            <input type='text' name='question[]' class='question-title-field' placeholder='Untitled Question'>
          </div>
        </div>
          <div class="card-body">
            <div>
            <div id='option' class='option'>
              <input type='radio' name='option' disabled>
              <input type='text' name='answer[]' class='option-field' placeholder='Option'>
            </div> 
            <div class='option'>
                <input type='radio' name='option' disabled>
                <input type='text' name='answer[]' class='option-field' placeholder='Option'>
              </div>
              <span id='addOptionsHere'></span>
            </div>
          <a onClick=(addOption()) class="tiny ui basic blue button">Add Option</a>
          <a onClick=(deleteOption()) class="tiny ui basic red button">Delete Option</a>
          <a onClick="this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);" class="ui icon basic button right floated">
            <i class="trash icon"></i>
          </a>
        </div>
      </div>
      <span id="addQuestionsHere"></span>
      {!! Form::submit('Submit Questionnaire', ['class' => 'submit-questionnaire-btn small ui blue button']) !!}
      {!! Form::close() !!}
  </div>
</div>
@endsection