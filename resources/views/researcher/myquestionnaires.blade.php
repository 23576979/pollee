@extends('layouts.master')

@section('title', 'My Questionnaires')

@section('content')
<div class='flex-container'>
  <div class='questionnaire-cards'>
      @if(\Session::has('success'))
      <div class='alert alert-success'>
      {{\Session::get('success')}}
      </div>
      @endif
      @if(\Session::has('questionnairedeleted'))
      <div class='alert alert-success'>
      {{\Session::get('questionnairedeleted')}}
      </div>
      @endif
      @if(\Session::has('questionnaireAdded'))
      <div class='alert alert-success'>
      {{\Session::get('questionnaireAdded')}}
      </div>
      @endif
    @if (isset ($questionnaires))
    <div class='row title-div'>
        <h2>My Questionnaires</h2>
          {{ Form::open(array('action' => 'QuestionnaireController@create', 'method' => 'get')) }}
          <div class="create-button">
            {!! Form::submit('Create Questionnaire', ['class' => 'btn small ui green button']) !!}
          </div>
          {{ Form::close() }}
        </div>
        <!-- foreach questionnaire create a new card and popualte each card with a title, description and who created it-->
          @foreach ($questionnaires as $questionnaire)
          <div class="ui fluid raised questionnaire-card card">
              <div class='card-header'>
                <h4>{{ $questionnaire->title}}</h4>
              </div>
              <div class="card-body">
                <p class="card-text">{{ $questionnaire->description}}</p>
                <div class='questionnaire-buttons'>
                <a href='/myquestionnaires/{{ $questionnaire->id}}/edit' name='{{ $questionnaire->id }}'><button class='edit-button small ui yellow button'>Edit</button></a>
                {{ Form::open(array('url' => 'myquestionnaires/' . $questionnaire->id)) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete', array('class' => 'btn small ui red button')) }}
                {{ Form::close() }}
                {{ Form::open(array('url' => 'myquestionnaires/responses/' . $questionnaire->id, 'action' => 'QuestionnaireController@show', 'method' => 'get')) }}
                  {!! Form::submit('View Responses', ['class' => 'responses-btn small ui blue button']) !!}
                {{ Form::close() }}
                </div>
              </div>
            </div>
          @endforeach
    @else
      <p> no questionnaires added yet </p>
    @endif
  </div>
</div>
@endsection