<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="company-name navbar-brand" href="/questionnaires">
  <img id='logo' src='/img/Logo-Single-Trans.png' />
  <span class='brand-title'>Pollee</span>
  </a>
  <button
    class="navbar-toggler"
    type="button"
    data-toggle="collapse"
    data-target="#navbarNav"
    aria-controls="navbarNav"
    aria-expanded="false"
    aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav mr-auto">
      <!-- If the user isnt signed in then only show the home page link-->
      @if (Auth::guest())
      <li class="nav-item">
        <a class="nav-link" href="/questionnaires">Questionnaires</a>
      </li>
      @else
      <!-- If the user is logged in then show the myquestionnaires link -->
      <li class='nav-item'>
        <a class='nav-link' href='/myquestionnaires'>My Questionnaires</a>
      </li>
      <!-- If the user is logged in and is an admin then show the admin home page link-->
      @if(Auth::user()->isAdmin)
      <li class='nav-item'>
        <a class='nav-link' href='/admin'>Users</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='/admin/reported_questionnaires'>Reported Questionnaires</a>
      </li>
      @endif
      @endif
    </ul>
    <ul class='navbar-nav ml-auto'>
      <!-- If the user isnt logged in then show the register and login links -->
      @if (Auth::guest())
      <li class="nav-item">
          <a class="nav-link" href="/register">Register</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/login">Login</a>
        </li>
      @else
      <!-- If the user is logged in then show the profile nae and a logout link -->
        {{ Auth::user()->name}}
          <a class='nav-link' href='/myprofile'>{{Auth::user()->first_name . ' ' . Auth::user()->last_name}}</a>
          <a class='nav-link' href='/logout'>Logout</a>
      @endif
      
    </ul>
</nav>