@extends('layouts.master')

@section('title', 'Pollee')

@section('content')
<div class='flex-container'>
  <div class='questionnaire-cards'>
      @if(\Session::has('error'))
      <div class='alert alert-danger'>
      {{\Session::get('error')}}
      </div>
      @endif
      @if(\Session::has('success'))
      <div class='alert alert-success'>
      {{\Session::get('success')}}
      </div>
      @endif
      @if(\Session::has('reported'))
      <div class='alert alert-success'>
      {{\Session::get('reported')}}
      </div>
      @endif
      <!-- if there are questionnaires in the db then create a title and a search box-->
    @if (isset ($questionnaires))
        <div class='home-title-section'>
          <h2>Questionnaires</h2>
          <div class="ui right floated search">
              <div class="ui icon input">
                <input id='search' onkeyup='questionnaireSearch()'  type="text" placeholder="Search questionnaires...">
                <i class="search icon"></i>
              </div>
              <div class="results"></div>
            </div>
          </div>
          <!-- foreach questionnaire in the db create a new card and add the title, description and created by field-->
          @foreach ($questionnaires as $questionnaire)
          <div id='title' class="ui fluid raised questionnaire-card card">
            <div id='questionnaireTitle' class='card-header'>
              <h4 >{{ $questionnaire->title}}</h4>
              </div>
                <div class="card-body">
                <div class='flex-card'>
                  <p class="card-text">{{ $questionnaire->description}}</p>
                </div>  
              </div>
              <div class="extra content">
                  <a href='/questionnaires/{{ $questionnaire->id}}' class="answer-btn small ui green button" name={{ $questionnaire->title }}>Answer</a>
              <a href='/questionnaires/report/{{$questionnaire->id}}' class='btn small ui red button'>Report</a>
                  <div class="right floated author">
                      <p class='card-text created-by text-right'>{{ $questionnaire->first_name}} {{ $questionnaire->last_name}}</p>
                  </div>
                </div>
            </div>
          @endforeach
    @else
      <p> no questionnaires added yet</p>
    @endif
  </div>
</div>
  @endsection