@extends('layouts.master')

@section('title', 'Answer Questionnaire')

@section('content')
    {!! Form::open(array('action' => 'HomeController@store', 'class' => 'answerQuestionnaire')) !!}
    <input name="questionnaire_id" type="hidden" value={{ $questionnaire->id}}>
    <div class='flex-container'>
      <div class='questionnaire-cards'>
        <div class='row title-div'>
          <h2>{{ $questionnaire->title }}</h2>
        </div>
        <div class='description-div'> 
          <p>{{ $questionnaire->description}}</p>
        </div>
        <!-- foreach question within the questionnaire, create a new card withe the questions id in a hidden input and the questions title in a h4-->
      @foreach($questions as $question)
      <input name="question_id[]" type="hidden" value={{ $question->id}}>
        <div class='ui raised fluid card'>
            <div class='card-header'>
              <h4 name="question_id" value={{ $question->id}} class='question-title-field'>{{ $question->question}}</h4>
            </div>
            <div class='card-body'>
              <!-- foreach answer if the current answers 'belongs_to_question' field matches the current question id then create a checkbox input for that answer with the answers value attatched-->
            @foreach($answers as $answer)
              @if($answer->belongs_to_question == $question->id)
              <div class='option-answer'>
                <input type='checkbox' name='answer_id[]' value={{ $answer->id}}><span class='answer-text'>{{$answer->answer}}</span></input>
              </div> 
              @endif
            @endforeach
          </div>
        </div>
      @endforeach
      {!! Form::submit('Submit Questionnaire', ['class' => 'submit-questionnaire-btn small ui blue button']) !!}
  {!! Form::close() !!}
    <div class="form-group row">
      <div class="col-sm-12">
        <a href='/questionnaires' class='small ui blue button'>Go Back</a>
      </div>
    </div>
  </div>
</div>
@endsection