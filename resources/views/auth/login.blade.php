@extends('layouts.master')
@section('title', 'Login')

@section('content')
<div class='login-container'>
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
                <h3 class="account-title card-header">{{ __('Login') }}</h3>
                <div class="card-body">
                    <form class='login' method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder='Email' value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder='Password' required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                          </div>
                        <div class="form-group row mb-0">
                          <div class="col-md-12">
                            <button type="submit" class="btn small ui fluid blue button">
                              {{ __('Login') }}
                            </button>
                          </div>
                        </div>
                           
                    </form>
                </div>
                @if (Route::has('password.request'))
           <p class='already-account'><a href="{{ route('password.request') }}">
            Forgot Your Password?
           </a></p>
          @endif
            </div>
        </div>
    </div>
  </div>
@endsection
