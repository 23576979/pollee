<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('admin/routes', 'HomeController@admin'->middleware('admin'));
//Home page route
Route::resource('/questionnaires', 'HomeController');
Route::get('/questionnaires/report/{id}', 'HomeController@report');

Route::group(['middleware' => ['admin']], function() {
  Auth::routes();
  Route::get('/admin', 'HomeController@admin');
  Route::delete('/admin/{id}', 'HomeController@destroy');
  Route::get('/admin/reported_questionnaires', 'QuestionnaireController@reportedQuestionnaires');
  Route::delete('/admin/reported_questionnaires/{id}', 'QuestionnaireController@deleteReported');
  Route::get('/admin/revoke/reported_questionnaire/{id}', 'QuestionnaireController@revoke');
});


Route::group(['middleware' => ['web']], function() {
  Auth::routes();
  Route::resource('/myquestionnaires', 'QuestionnaireController');
  Route::resource('/myquestionnaires/responses', 'QuestionnaireController');
  Route::resource('/myquestionnaires/edit', 'QuestionnaireController');
  Route::resource('/myprofile', 'ProfileController');
  Route::get('/myquestionnaires/responses/export/{id}', 'QuestionnaireController@export');
  });



Route::get('logout', 'Auth\LoginController@logout');